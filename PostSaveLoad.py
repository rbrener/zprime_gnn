import torch
import dgl
from dgl.data.utils import load_graphs
from torch.utils.data import Dataset, DataLoader
from tqdm import tqdm


################################
######Load from .bin files######
################################

training_data_loader_graphs, training_data_loader_labels_and_indices = load_graphs("training_data.bin")
training_data_loader_labels  = training_data_loader_labels_and_indices['type'].tolist()
training_data_loader_indices = training_data_loader_labels_and_indices['index'].tolist()

validation_data_loader_graphs, validation_data_loader_labels_and_indices = load_graphs("validation_data.bin")
validation_data_loader_labels  = validation_data_loader_labels_and_indices['type'].tolist()
validation_data_loader_indices = validation_data_loader_labels_and_indices['index'].tolist()

testing_data_loader_graphs, testing_data_loader_labels_and_indices = load_graphs("testing_data.bin")
testing_data_loader_labels  = testing_data_loader_labels_and_indices['type'].tolist()
testing_data_loader_indices = testing_data_loader_labels_and_indices['index'].tolist()


################################
######Convert to lists #########
################################

training_data_loader_graphs_dic_list = []
training_data_loader_labels_dic_list = []
training_data_loader_idx_dic_list = []

validation_data_loader_graphs_dic_list = []
validation_data_loader_labels_dic_list = []
validation_data_loader_idx_dic_list = []


testing_data_loader_graphs_dic_list = []
testing_data_loader_labels_dic_list = []
testing_data_loader_idx_dic_list = []


for i in tqdm(range(len(training_data_loader_graphs))):
  training_data_loader_graphs_dic_list.append( {'gr' : training_data_loader_graphs[i]} )
  training_data_loader_labels_dic_list.append( {'type': training_data_loader_labels[i]} )
  training_data_loader_idx_dic_list.append( {'index': training_data_loader_indices[i]} )

for j in tqdm(range(len(validation_data_loader_graphs))):
  validation_data_loader_graphs_dic_list.append( {'gr' : validation_data_loader_graphs[j]} )
  validation_data_loader_labels_dic_list.append( {'type': validation_data_loader_labels[j]} )
  validation_data_loader_idx_dic_list.append( {'index': validation_data_loader_indices[j]} )

for k in tqdm(range(len(testing_data_loader_graphs))):
  testing_data_loader_graphs_dic_list.append( {'gr' : testing_data_loader_graphs[k]} )
  testing_data_loader_labels_dic_list.append( {'type' : testing_data_loader_labels[k]} )
  testing_data_loader_idx_dic_list.append( {'index' : testing_data_loader_indices[k]} )


###################################
######Bring to desired format######
###################################

#merge dics in list s.t. every list contains dics of graph and label

training_dataset_loaded = []
validation_dataset_loaded = []

testing_dataset_loaded = []

for i in tqdm(range(len(training_data_loader_graphs_dic_list))):
  training_data_loader_graphs_dic_list[i].update(training_data_loader_labels_dic_list[i])
  training_data_loader_graphs_dic_list[i].update(training_data_loader_idx_dic_list[i])

for j in tqdm(range(len(validation_data_loader_graphs_dic_list))):
  validation_data_loader_graphs_dic_list[j].update(validation_data_loader_labels_dic_list[j])
  validation_data_loader_graphs_dic_list[j].update(validation_data_loader_idx_dic_list[j])

for k in tqdm(range(len(testing_data_loader_graphs_dic_list))):
  testing_data_loader_graphs_dic_list[k].update(testing_data_loader_labels_dic_list[k])
  testing_data_loader_graphs_dic_list[k].update(testing_data_loader_idx_dic_list[k])


training_dataset_loaded   = training_data_loader_graphs_dic_list
validation_dataset_loaded = validation_data_loader_graphs_dic_list

testing_dataset_loaded = testing_data_loader_graphs_dic_list

torch.save(training_dataset_loaded, "training_dataset_loaded.pt")
torch.save(validation_dataset_loaded, "validation_dataset_loaded.pt")


torch.save(testing_dataset_loaded, "testing_dataset_loaded.pt")

#here we have the desired format saved to collate



