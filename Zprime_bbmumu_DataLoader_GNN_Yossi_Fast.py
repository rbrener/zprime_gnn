#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  9 18:36:05 2021

@author: rbrener
"""

import numpy as np
import sys,math
import array
import os
import random
import uproot
import uproot_methods
import torch
from torch.utils.data import Dataset, DataLoader
from tqdm import tqdm
import glob
import json
import ast
import dgl


def GetMuonPairIndices(pt,q):
    mu_pluses  = []
    mu_minuses = []
    pt_index_dict = {}
    for mu in range(len(pt)):
        pt_index_dict[ pt[mu] ] = mu
        if q[mu]>0:   mu_pluses.append(pt[mu])
        elif q[mu]<0: mu_minuses.append(pt[mu])
        else:
           print("q(muon)=0, quitting")
           quit()

    if (len(mu_pluses) > 0): Zprime_mu_plus_idx = pt_index_dict[ max(mu_pluses) ] 
    else: Zprime_mu_plus_idx = -1

    if (len(mu_minuses) > 0): Zprime_mu_minus_idx = pt_index_dict[ max(mu_minuses) ] 
    else: Zprime_mu_minus_idx = -1
    
    return Zprime_mu_plus_idx, Zprime_mu_minus_idx

def GetSortedJetsIndices(pt):
    pt_index_dict = {}
    for jet in range(len(pt)): pt_index_dict[ pt[jet] ] = jet
    jet_index_ptsorted = []
    for jet_pt in sorted(pt_index_dict): jet_index_ptsorted.append( pt_index_dict[jet_pt] )
    return jet_index_ptsorted




class ZprimeMuMuDataset_DGLGraph(Dataset):
    def __init__(self, path, num_ev=-1):
        super().__init__()
        filelist = glob.glob(path+'/*.root')
        self.fbkg = 0
        self.fsig = 0
        
        for fname in filelist:
            print (fname)
            if ('ttbar' in fname or 'DY_' in fname or 'Diboson_' in fname):
               self.fbkg = uproot.open(fname)
               #self.event_tree_bkg = self.fbkg['nominal_MuMu_jb'].arrays(self.fbkg['nominal_MuMu_jb'].keys(), library='np')
               some_entry_last = self.fbkg['nominal_MuMu_jb'].num_entries
               #some_entry_start = some_entry_last-4000
               #some_entry_finish = some_entry_last-1000
               self.event_tree_bkg = self.fbkg['nominal_MuMu_jb'].arrays(self.fbkg['nominal_MuMu_jb'].keys(), entry_start=0, entry_stop=some_entry_last, library='np')
               
            else:
               self.fsig = uproot.open(fname)
               self.event_tree_sig = self.fsig['nominal_MuMu_jb'].arrays(self.fsig['nominal_MuMu_jb'].keys(), entry_stop=self.fsig['nominal_MuMu_jb'].num_entries, library='np')

        print ("filelist:  ", filelist)
 
        self.good_events = []

        #bkg_sample_size = self.fbkg['nominal_MuMu_jb'].num_entries

        ####Assemble good events (=must contain dilepton objects and b-jet/s) indices###
        if 'train' in path or 'test' in path:
            #sample_size = 7000
            sample_size = self.fbkg['nominal_MuMu_jb'].num_entries
            #sample_size = some_entry_finish-some_entry_start


        elif 'valid' in path:
            sample_size = 70000/4
        else:
            sample_size = 70000



        min_mass = 70
        for i in tqdm(range(0, self.fsig['nominal_MuMu_jb'].num_entries)):
            sigflag=1.0
            try:
                #ll_flag = len(self.event_tree_sig['m_ll'][i])
                bjet_len = (self.event_tree_sig['n_bjet'][i])
                m_ll = self.event_tree_sig['m_ll'][i]
            except Exception:
                continue
            if (bjet_len == 0 or m_ll < min_mass):
                continue
            self.good_events.append((i, sigflag))
            
        for i in tqdm(range(0, int(sample_size))):
            sigflag=0.0
            try:
                #ll_flag = len(self.event_tree_bkg['m_ll'].array(entry_start=i,entry_stop=i+1,library='np'))
                bjet_len = self.event_tree_bkg['n_bjet'][i]
                m_ll = self.event_tree_bkg['m_ll'][i]
            except Exception:
                continue
            if (bjet_len == 0 or m_ll < min_mass):
                continue
            self.good_events.append((i, sigflag))
        
        ##########Save Mean, RMS values once per tree##########
        global MeanRMS_SigDict, MeanRMS_BkgDict
        MeanRMS_SigDict = {}
        MeanRMS_BkgDict = {}
        
        normalise_flag = 1

        if normalise_flag == 1:

          if 'train' in path or 'test' in path:
            
              MeanRMS_SigDict['mu_pt'] = self.getMeanandRMS(self.event_tree_sig,'leading_l_pt')
              MeanRMS_SigDict['mu_eta'] = self.getMeanandRMS(self.event_tree_sig,'leading_l_eta')
              MeanRMS_SigDict['mu_phi'] = self.getMeanandRMS(self.event_tree_sig,'leading_l_phi')
              MeanRMS_SigDict['mu_e'] = self.getMeanandRMS(self.event_tree_sig,'leading_l_e')
              MeanRMS_SigDict['jb_pt'] = self.getMeanandRMS(self.event_tree_sig,'leading_bjet_pt')
              MeanRMS_SigDict['jb_eta'] = self.getMeanandRMS(self.event_tree_sig,'leading_bjet_eta')
              MeanRMS_SigDict['jb_phi'] = self.getMeanandRMS(self.event_tree_sig,'leading_bjet_phi')
              MeanRMS_SigDict['jb_e'] = self.getMeanandRMS(self.event_tree_sig,'leading_bjet_e')

              MeanRMS_SigDict['m_ll'] = self.getMeanandRMS(self.event_tree_sig,'m_ll')
              MeanRMS_SigDict['pt_ll'] = self.getMeanandRMS(self.event_tree_sig,'pt_ll')

              
              MeanRMS_BkgDict = {}
              MeanRMS_BkgDict['mu_pt'] = self.getMeanandRMS(self.event_tree_bkg,'leading_l_pt')
              MeanRMS_BkgDict['mu_eta'] = self.getMeanandRMS(self.event_tree_bkg,'leading_l_eta')
              MeanRMS_BkgDict['mu_phi'] = self.getMeanandRMS(self.event_tree_bkg,'leading_l_phi')
              MeanRMS_BkgDict['mu_e'] = self.getMeanandRMS(self.event_tree_bkg,'leading_l_e')
              MeanRMS_BkgDict['jb_pt'] = self.getMeanandRMS(self.event_tree_bkg,'leading_bjet_pt')
              MeanRMS_BkgDict['jb_eta'] = self.getMeanandRMS(self.event_tree_bkg,'leading_bjet_eta')
              MeanRMS_BkgDict['jb_phi'] = self.getMeanandRMS(self.event_tree_bkg,'leading_bjet_phi')
              MeanRMS_BkgDict['jb_e'] = self.getMeanandRMS(self.event_tree_bkg,'leading_bjet_e')

              MeanRMS_BkgDict['m_ll'] = self.getMeanandRMS(self.event_tree_bkg,'m_ll')
              MeanRMS_BkgDict['pt_ll'] = self.getMeanandRMS(self.event_tree_bkg,'pt_ll')
      
              MeanRMS_SigDict_file = open("MeanRMS_SigDict_file.json", "w")
              json.dump(str(MeanRMS_SigDict), MeanRMS_SigDict_file)
              MeanRMS_SigDict_file.close()

              MeanRMS_BkgDict_file = open("MeanRMS_BkgDict_file.json", "w")
              json.dump(str(MeanRMS_BkgDict), MeanRMS_BkgDict_file)
              MeanRMS_BkgDict_file.close()
          
          elif 'valid' in path:
            MeanRMS_SigDict_file = open("MeanRMS_SigDict_file.json", "r")
            bare_output_sig = MeanRMS_SigDict_file.read()
            string_dict_sig = json.loads(bare_output_sig)
            MeanRMS_SigDict = ast.literal_eval(string_dict_sig)
            MeanRMS_SigDict_file.close()

            MeanRMS_BkgDict_file = open("MeanRMS_BkgDict_file.json", "r")
            bare_output_bkg = MeanRMS_BkgDict_file.read()
            string_dict_bkg = json.loads(bare_output_bkg)
            MeanRMS_BkgDict = ast.literal_eval(string_dict_bkg)
            MeanRMS_BkgDict_file.close()
    
    
    
    def __len__(self):
        return len(self.good_events)
        
    def __getitem__(self, idx):
        to_load = self.good_events[idx]
        if isinstance(idx, int):
            return self.get_single_event(*to_load)
            #return pd.concat([self.get_single_event(*event) for event in to_load])
        return [self.get_single_event(*event) for event in to_load]
        #return pd.concat([self.get_single_event(*event) for event in to_load])
    
    
    def getMeanandRMS(self, tree, var):

        dist       = tree[var]
        #array_dist = dist.array(library='np')
        #list_dist  = [item for sublist in array_dist for item in sublist]
        mean = np.mean(dist)
        RMS  = np.std(dist)
    
        return mean, RMS
    
    
    def get_single_event(self, event_idx, sigflag):

        if sigflag: 
            event_tree = self.event_tree_sig
        else:
            event_tree = self.event_tree_bkg
            
        pt_mu1 = event_tree['leading_l_pt'][event_idx]
        eta_mu1 = event_tree['leading_l_eta'][event_idx]
        phi_mu1 = event_tree['leading_l_phi'][event_idx]
        E_mu1 = event_tree['leading_l_e'][event_idx]
        
        pt_mu2 = event_tree['subleading_l_pt'][event_idx]
        eta_mu2 = event_tree['subleading_l_eta'][event_idx]
        phi_mu2 = event_tree['subleading_l_phi'][event_idx]
        E_mu2 = event_tree['subleading_l_e'][event_idx]

        pt_jb1 = event_tree['leading_bjet_pt'][event_idx]
        eta_jb1 = event_tree['leading_bjet_eta'][event_idx]
        phi_jb1 = event_tree['leading_bjet_phi'][event_idx]
        E_jb1 = event_tree['leading_bjet_e'][event_idx]
        
        Zprime_pt = event_tree['pt_ll'][event_idx]
        Zprime_eta = event_tree['eta_ll'][event_idx]
        Zprime_phi = event_tree['phi_ll'][event_idx]
        Zprime_e = event_tree['e_ll'][event_idx]

        Z_prime_m = event_tree['m_ll'][event_idx]
       
        jet_flag = event_tree['n_bjet'][event_idx]
 
        if (jet_flag > 1):
            
            pt_jb2 = event_tree['subleading_bjet_pt'][event_idx]
            eta_jb2 = event_tree['subleading_bjet_eta'][event_idx]
            phi_jb2 = event_tree['subleading_bjet_phi'][event_idx]
            E_jb2 = event_tree['subleading_bjet_e'][event_idx]

        if (jet_flag == 0):
            return None
  
        
        Zp_LV = uproot_methods.TLorentzVectorArray.from_ptetaphie([Zprime_pt], [Zprime_eta], [Zprime_phi], [Zprime_e]) 
        boost_vector = (Zp_LV.p3/Zp_LV.p3.mag)*Zp_LV.beta
 
        
        mu2_LV = uproot_methods.TLorentzVectorArray.from_ptetaphie([pt_mu2], [eta_mu2], [phi_mu2], [E_mu2])
        jb1_LV = uproot_methods.TLorentzVectorArray.from_ptetaphie([pt_jb1], [eta_jb1], [phi_jb1], [E_jb1])
        
        
        if jet_flag == 1:
            src_ids = torch.tensor([0, 0, 1, 1, 2, 2])
            dst_ids = torch.tensor([1, 2, 0, 2, 0, 1])
            gr = dgl.graph((src_ids, dst_ids))
            gr.ndata['features'] = torch.randn(3,4)
        
        elif jet_flag > 1:
            src_ids = torch.tensor([0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3])
            dst_ids = torch.tensor([1, 2, 3, 0, 2, 3, 0, 1, 3, 0, 1, 2])
            gr = dgl.graph((src_ids, dst_ids))
            
            gr.ndata['features'] = torch.randn(4,4)


      
        ############################
        ######Normalise inputs######
        ############################

        ######Get mean, RMS of muons, jets######
        normalise_flag = 1

        if normalise_flag == 1:

          if sigflag:
              MeanRMSDict = MeanRMS_SigDict
          else:
              MeanRMSDict = MeanRMS_BkgDict

          Z_prime_m_mean, Z_prime_m_RMS = MeanRMSDict['m_ll']
          Z_prime_pt_mean, Z_prime_pt_RMS = MeanRMSDict['pt_ll']
          
          pt_mu_mean,pt_mu_RMS     = MeanRMSDict['mu_pt']
          eta_mu_mean,eta_mu_RMS   = MeanRMSDict['mu_eta']
          phi_mu_mean,phi_mu_RMS   = MeanRMSDict['mu_phi']
          e_mu_mean,e_mu_RMS       = MeanRMSDict['mu_e']
          
          pt_jb_mean,pt_jb_RMS     = MeanRMSDict['jb_pt']
          eta_jb_mean,eta_jb_RMS   = MeanRMSDict['jb_eta']
          phi_jb_mean,phi_jb_RMS   = MeanRMSDict['jb_phi']
          e_jb_mean,e_jb_RMS       = MeanRMSDict['jb_e']
        ######Normalise inputs######
        
          pt_mu1_norm = np.abs(pt_mu1 - pt_mu_mean)/pt_mu_RMS
          eta_mu1_norm = (eta_mu1 - eta_mu_mean)/eta_mu_RMS
          phi_mu1_norm = (phi_mu1 - phi_mu_mean)/pt_mu_RMS
          e_mu1_norm = np.abs(E_mu1 - e_mu_mean)/e_mu_RMS
          
          pt_mu2_norm = np.abs(pt_mu2 - pt_mu_mean)/pt_mu_RMS
          eta_mu2_norm = (eta_mu2 - eta_mu_mean)/eta_mu_RMS
          phi_mu2_norm = (phi_mu2 - phi_mu_mean)/pt_mu_RMS
          e_mu2_norm = np.abs(E_mu2 - e_mu_mean)/e_mu_RMS
          
          pt_jb1_norm = np.abs(pt_jb1 - pt_jb_mean)/pt_jb_RMS
          eta_jb1_norm = (eta_jb1 - eta_jb_mean)/eta_jb_RMS
          phi_jb1_norm = (phi_jb1 - phi_jb_mean)/pt_jb_RMS
          e_jb1_norm = np.abs(E_jb1 - e_jb_mean)/e_jb_RMS
        ###########################
        
          Zprime_m_norm = np.abs(Z_prime_m - Z_prime_m_mean)/Z_prime_m_RMS
          Zprime_pt_norm = np.abs(Zprime_pt - Z_prime_pt_mean)/Z_prime_pt_RMS

        ######Fill event tensor######
          if jet_flag > 1:
              
              ######Normalise inputs of jb2######
              pt_jb2_norm = np.abs(pt_jb2 - pt_jb_mean)/pt_jb_RMS
              eta_jb2_norm = (eta_jb2 - eta_jb_mean)/eta_jb_RMS
              phi_jb2_norm = (phi_jb2 - phi_jb_mean)/pt_jb_RMS
              e_jb2_norm = np.abs(E_jb2 - e_jb_mean)/e_jb_RMS
              jb2_LV = uproot_methods.TLorentzVectorArray.from_ptetaphie([pt_jb2_norm], [eta_jb2_norm], [phi_jb2_norm], [e_jb2_norm])


########ONLY FOR VERIFICATION ------- INSERT UNNORMALISED VALUES#############

        gr.ndata['features'][0] = torch.tensor([pt_mu1_norm, eta_mu1_norm, phi_mu1_norm, e_mu1_norm])
        mu1_LV = uproot_methods.TLorentzVectorArray.from_ptetaphim([pt_mu1_norm], [eta_mu1_norm], [phi_mu1_norm], [e_mu1_norm])
        gr.ndata['features'][1] = torch.tensor([pt_mu2_norm, eta_mu2_norm, phi_mu2_norm, e_mu2_norm])
        mu2_LV = uproot_methods.TLorentzVectorArray.from_ptetaphim([pt_mu2_norm], [eta_mu2_norm], [phi_mu2_norm], [e_mu2_norm])
        gr.ndata['features'][2] = torch.tensor([pt_jb1_norm, pt_jb1_norm, phi_jb1_norm, e_jb1_norm])
        jb1_LV = uproot_methods.TLorentzVectorArray.from_ptetaphim([pt_jb1_norm], [pt_jb1_norm], [phi_jb1_norm], [e_jb1_norm])

        
        if jet_flag > 1:

            gr.ndata['features'][3] = torch.tensor([pt_jb2_norm, eta_jb2_norm, phi_jb2_norm, e_jb2_norm])
            jb2_LV = uproot_methods.TLorentzVectorArray.from_ptetaphim([pt_jb2_norm], [eta_jb2_norm], [phi_jb2_norm], [e_jb2_norm])
            gr.edata['features'] = torch.randn(gr.number_of_edges(),2)
            
        elif jet_flag == 1:
            gr.edata['features'] = torch.randn(gr.number_of_edges(),2)


        for edge_idx in range(gr.number_of_edges()):
            U = uproot_methods.TLorentzVectorArray.from_ptetaphie( [gr.ndata['features'][gr.find_edges(edge_idx)[0]][0][0].item()], [gr.ndata['features'][gr.find_edges(edge_idx)[0]][0][1].item()], [gr.ndata['features'][gr.find_edges(edge_idx)[0]][0][2].item()], [gr.ndata['features'][gr.find_edges(edge_idx)[0]][0][3].item()] )
            V = uproot_methods.TLorentzVectorArray.from_ptetaphie( [gr.ndata['features'][gr.find_edges(edge_idx)[1]][0][0].item()], [gr.ndata['features'][gr.find_edges(edge_idx)[1]][0][1].item()], [gr.ndata['features'][gr.find_edges(edge_idx)[1]][0][2].item()], [gr.ndata['features'][gr.find_edges(edge_idx)[1]][0][3].item()] )

            deltaR = U.delta_r(V)

            try:
                -boost_vector[0].x
            except Exception:
                continue

            U_boost = U[0].boost(-boost_vector[0])
            V_boost = V[0].boost(-boost_vector[0])

            costheta_star = (U_boost.p3.dot(V_boost.p3))/(U_boost.p*V_boost.p)

            gr.edata['features'][edge_idx] = torch.tensor([deltaR[0], costheta_star])



        return {'gr' : gr,  
                'type': sigflag, 'index': event_idx}
        
