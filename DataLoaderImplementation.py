from Zprime_bbmumu_DataLoader_GNN_Yossi_Fast import ZprimeMuMuDataset_DGLGraph
from tqdm import tqdm
import dgl
import torch
import networkx as nx
from dgl.data.utils import save_graphs

###################################
####Implement custom dataloader####
###################################

training_dataset = ZprimeMuMuDataset_DGLGraph('/efs/Roy/Datasets/ROOT_Files/training/')
validation_dataset = ZprimeMuMuDataset_DGLGraph('/efs/Roy/Datasets/ROOT_Files/validation/')

testing_dataset = ZprimeMuMuDataset_DGLGraph('/efs/Roy/Datasets/ROOT_Files/testing/')

###################################
#######Separate into lists#########
###################################

training_labels_list = []
training_graphs_list = []
training_indices_list = []
validation_labels_list = []
validation_graphs_list = []
validation_indices_list = []


testing_labels_list = []
testing_graphs_list = []
testing_indices_list = []


for i in tqdm(training_dataset, position=0):
  if i is None: continue
  training_labels_list.append(i['type'])
  training_graphs_list.append(i['gr'])
  training_indices_list.append(i['index'])

for j in tqdm(validation_dataset, position=0):
  if j is None: continue
  validation_labels_list.append(j['type'])
  validation_graphs_list.append(j['gr'])
  validation_indices_list.append(j['index'])

for k in tqdm(testing_dataset, position=0):
  #if k in None: continue
  testing_labels_list.append(k['type'])
  testing_graphs_list.append(k['gr'])
  testing_indices_list.append(k['index'])


###################################
######Lists to torch tensors#######
###################################

training_labels   = {"type": torch.tensor(training_labels_list)}
validation_labels = {"type": torch.tensor(validation_labels_list)}

testing_labels = {"type": torch.tensor(testing_labels_list)}


training_indices   = {"index": torch.tensor(training_indices_list)}
validation_indices = {"index": torch.tensor(validation_indices_list)}

testing_indices = {"index": torch.tensor(testing_indices_list)}

training_labels_and_indices = {"type": torch.tensor(training_labels_list), "index": torch.tensor(training_indices_list)}
validation_labels_and_indices = {"type": torch.tensor(validation_labels_list), "index": torch.tensor(validation_indices_list)}

testing_labels_and_indices = {"type": torch.tensor(testing_labels_list), "index": torch.tensor(testing_indices_list)}


###################################
#####Save graphs as .bin files#####
###################################
#for docker add full path upon saving: /efs/Roy/zprime_gnn/

save_graphs("./training_data.bin", training_graphs_list, training_labels_and_indices)
save_graphs("./validation_data.bin", validation_graphs_list, validation_labels_and_indices)

save_graphs("./testing_data.bin", testing_graphs_list, testing_labels_and_indices)
