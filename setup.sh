#!/bin/bash

echo "pip-installing dgl"
echo "=================="
echo "=================="
pip3 install dgl-cu113 -f https://data.dgl.ai/wheels/repo.html 
echo "pip-installing uproot"
echo "=================="
echo "=================="
pip3 install uproot awkward
echo "pip-installing uproot-methods"
echo "=================="
echo "=================="
pip3 install uproot-methods
echo "++++++++++++++++++"
echo "Finished installing preambles"
pip3 install tqdm 
pip3 install torch
pip3 install matplotlib
pip3 install numpy

#export DGLBACKEND=pytorch

