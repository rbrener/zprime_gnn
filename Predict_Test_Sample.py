import torch
import dgl
from torch.utils.data import Dataset, DataLoader
import torch.nn as nn
import torch.optim as optim
from tqdm import tqdm
from model import Classifier
import json
import math
import numpy as np
from matplotlib import pyplot as plt

##########################################
##############Define sigmoid##############
##########################################

def sigmoid(x):
  try:
    ans = math.exp(x)
    return 1 / (1 + math.exp(-x))
  except OverflowError:
    x = float('inf')
    return 1 / (1 + math.exp(-x))


##########################################
######Define collate graphs function######
##########################################

def collate_graphs(samples):
  graph_list = []
  label_list = []
  index_list = []
  for sample in samples:
    graph_list.append(sample['gr'])
    label_list.append(sample['type'])
    index_list.append(sample['index'])

  batched_graph = dgl.batch(graph_list)
  batched_label = torch.tensor(label_list)
  batched_index = torch.tensor(index_list)
  return batched_graph, batched_label, batched_index

##########################################
########Define f1,loss function###########
##########################################

def compute_f1_and_loss(dataloader,net):
    
    true_pos = 0
    false_pos = 0
    false_neg = 0

    loss = 0
    
    all_targets     = []
    all_preds       = []
    all_indices     = []

    if torch.cuda.is_available():
        net.cuda()
    net.eval()
    
    n_batches = 0
    with torch.no_grad():
        for batched_g, batched_label, batched_index in tqdm(dataloader):
            #if n_batches % 10 !=0:
            #  continue

            n_batches+=1

            if torch.cuda.is_available():
                batched_g = batched_g.to(torch.device('cuda'))
                batched_label = batched_label.to(torch.device('cuda'))
                batched_index = batched_index.to(torch.device('cuda'))

            #print ("batched_index:   ", batched_index)
            
            target = batched_label.unsqueeze(-1)
            pred = net(batched_g)

            loss+= loss_func(pred,target).item()

            true_pos+=len(torch.where( (pred>0) & (target==1) )[0])
            false_pos+=len(torch.where( (pred>0) & (target==0) )[0])
            false_neg+=len(torch.where( (pred<0) & (target==1) )[0])

            all_targets.append(target)
            all_preds.append(pred)
            all_indices.append(batched_index)


    f1 = true_pos/(true_pos+0.5*(false_pos+false_neg))

    loss = loss/n_batches      
    return f1, loss, all_targets, all_preds, all_indices


##########################################
############Load saved data###############
##########################################

testing_dataset_loaded = torch.load("testing_dataset_loaded.pt")
#testing_dataset_loaded = torch.load("fake_testing_dataset_loaded.pt")
print ("Loaded testing dataset")

##########################################
#############Collate objects##############
##########################################

testing_data_loader = DataLoader(testing_dataset_loaded, batch_size=256, shuffle=True, collate_fn=collate_graphs)


print ("Testing data is collated!")


##########################################
############Load trained net##############
##########################################

trained_net = Classifier()
trained_net.load_state_dict(torch.load('trained_model.pt'))
loss_func = nn.BCEWithLogitsLoss(pos_weight=torch.tensor(10.0))
print ("Loaded trained model")

##########################################
############Actually predict##############
##########################################

test_f1, test_loss, all_test_targets, all_test_preds, all_test_indices =  compute_f1_and_loss(testing_data_loader,trained_net)

print ("Assigned predictions to testing data!")

#################################################
#######Save predictions in desired format########
#################################################

all_preds = []
all_targets = []

all_indices = []

for idx in all_test_indices:
  all_indices.append(idx.tolist())

for t in all_test_targets:
  all_targets.append(t.tolist())

for ten in all_test_preds:
  all_preds.append(ten.tolist())


all_targets = sum( sum(all_targets, []), [] )
all_preds = sum( sum(all_preds, []), [] )
all_indices = sum(all_indices, [])



sig_preds = []
bkg_preds = []

sig_indices = []
bkg_indices = []

sig_preds_indices = []
bkg_preds_indices = []

for idx in range(len(all_targets)):
  
  if all_targets[idx] == 0.0:
    #print (all_targets[idx])
    bkg_preds.append(sigmoid( all_preds[idx] ) )
    bkg_indices.append(all_indices[idx])
    bkg_preds_indices.append({'GNN_Score': sigmoid( all_preds[idx]), 'event_idx': all_indices[idx]})

  else:
    sig_preds.append(sigmoid ( all_preds[idx]) )
    sig_indices.append(all_indices[idx])
    sig_preds_indices.append({'GNN_Score': sigmoid( all_preds[idx]), 'event_idx': all_indices[idx]})


bkg_preds_indices = sorted(bkg_preds_indices, key=lambda k: k['event_idx']) 
sig_preds_indices = sorted(sig_preds_indices, key=lambda k: k['event_idx'])


print ("Predictions converted to desired format!")


##########################################
##################Save####################
##########################################

print ("Saving test targets, predictions and indices to .json files")

json_file = open("test_bkg_preds_indices.json", "w")
json.dump(str(bkg_preds_indices), json_file)
json_file.close()

json_file = open("test_sig_preds_indices.json", "w")
json.dump(str(sig_preds_indices), json_file)
json_file.close()


########################################
###############Plot#####################
########################################

fig = plt.figure()
fig.patch.set_facecolor('white')
fig.patch.set_alpha(0.7)

ax = fig.add_subplot(111)
ax.patch.set_facecolor('white')
ax.patch.set_alpha(1.0)
ax.set_yscale('log')

n_sig,bins_sig, _ = plt.hist(sig_preds,bins=30, label='Signal',color='orange', alpha=0.5, density=False)
n_bkg,bins_bkg, _ = plt.hist(bkg_preds,bins=30, label='Background',color='teal', alpha=0.5, density=False)


plt.xlabel('GNN score')
plt.ylabel('Events')

plt.legend()


plt.savefig('scores.png')


