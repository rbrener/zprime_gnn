import torch
import dgl
from torch.utils.data import Dataset, DataLoader
import torch.nn as nn
import torch.optim as optim
from tqdm import tqdm
from model import Classifier


##########################################
######Define collate graphs function######
##########################################

def collate_graphs(samples):
  graph_list = []
  label_list = []
  index_list = []
  for sample in samples:
    graph_list.append(sample['gr'])
    label_list.append(sample['type'])
    index_list.append(sample['index'])

  batched_graph = dgl.batch(graph_list)
  batched_label = torch.tensor(label_list)
  batched_index = torch.tensor(index_list)
  return batched_graph, batched_label, batched_index


##########################################
############Load saved data###############
##########################################

training_dataset_loaded = torch.load("training_dataset_loaded.pt")
validation_dataset_loaded = torch.load("validation_dataset_loaded.pt")

print ("Loaded training, validation datasets")

##########################################
#############Collate objects##############
##########################################

data_loader = DataLoader(training_dataset_loaded, batch_size=256, shuffle=True, collate_fn=collate_graphs)
validation_data_loader = DataLoader(validation_dataset_loaded, batch_size=256, shuffle=True, collate_fn=collate_graphs)

print ("Data is collated!")

##########################################
##########Graphs on cuda machine##########
##########################################
for batched_g, batched_label, batched_index in data_loader:
  batched_g, batched_label, batched_index = batched_g.to(device='cuda'), batched_label.to(device='cuda'), batched_index.to(device='cuda')
  break

print ("Able to have objects on cuda device!")
print ("::::::::::::::::::::::::::::::::::::")
print ("Initiating training!")

##########################################
#################Train####################
##########################################

net = Classifier()
net.cuda()

loss_func = nn.BCEWithLogitsLoss(pos_weight=torch.tensor(10.0))
optimizer = optim.Adam(net.parameters(), lr=0.008)

###########Define inner function##########

def compute_f1_and_loss(dataloader,net):
    
    true_pos = 0
    false_pos = 0
    false_neg = 0

    loss = 0
    
    all_targets     = []
    all_preds       = []
    all_indices     = []

    if torch.cuda.is_available():
        net.cuda()
    net.eval()
    
    n_batches = 0
    with torch.no_grad():
        for batched_g, batched_label, batched_index in tqdm(dataloader):
            #if n_batches % 10 !=0:
            #  continue

            n_batches+=1

            if torch.cuda.is_available():
                batched_g = batched_g.to(torch.device('cuda'))
                batched_label = batched_label.to(torch.device('cuda'))
                batched_index = batched_index.to(torch.device('cuda'))

            #print ("batched_index:   ", batched_index)
            
            target = batched_label.unsqueeze(-1)
            pred = net(batched_g)

            loss+= loss_func(pred,target).item()

            true_pos+=len(torch.where( (pred>0) & (target==1) )[0])
            false_pos+=len(torch.where( (pred>0) & (target==0) )[0])
            false_neg+=len(torch.where( (pred<0) & (target==1) )[0])

            all_targets.append(target)
            all_preds.append(pred)
            all_indices.append(batched_index)


    f1 = true_pos/(true_pos+0.5*(false_pos+false_neg))

    loss = loss/n_batches      
    return f1, loss, all_targets, all_preds, all_indices


#############Define lists############

training_loss_vs_epoch = []
validation_loss_vs_epoch = []

training_f1_vs_epoch = []
validation_f1_vs_epoch = []



###########Actually train###########


print ("Training begins now!")

n_epochs = 100


pbar = tqdm( range(n_epochs) )

for epoch in pbar: 
    
    if len(validation_loss_vs_epoch) > 1:
        pbar.set_description(
              ' val f1:'+'{0:.5f}'.format(validation_f1_vs_epoch[-1]) +  
              'val loss:'+ str(validation_loss_vs_epoch[-1])
              )
        
    net.train() # put the net into "training mode"
    n_batches=0
    for batched_g, batched_label, batched_index in data_loader:
        n_batches+=1
        #if n_batches % 4 != 0:
        #  continue
        if torch.cuda.is_available():
            batched_g = batched_g.to(torch.device('cuda'))
            batched_label = batched_label.to(torch.device('cuda'))
            batched_index = batched_index.to(torch.device('cuda'))
            
            
        optimizer.zero_grad()
        target = batched_label.unsqueeze(-1)
        pred = net(batched_g)
        
        loss = loss_func(pred,target)
        loss.backward()
        optimizer.step()
    
    net.eval() #put the net into evaluation mode
    train_f1, train_loss, all_train_targets, all_train_preds, all_train_indices, = compute_f1_and_loss(data_loader,net)
    valid_f1, valid_loss, all_valid_targets, all_valid_preds, all_valid_indices =  compute_f1_and_loss(validation_data_loader,net)
         
    training_loss_vs_epoch.append(train_loss)    
    training_f1_vs_epoch.append( train_f1 )
    
    validation_loss_vs_epoch.append(valid_loss)
    validation_f1_vs_epoch.append(valid_f1)


    if len(validation_loss_vs_epoch)==1 or validation_loss_vs_epoch[-2] > validation_loss_vs_epoch[-1]:
        torch.save(net.state_dict(), 'trained_model.pt')

##########################################################################
###########Plot losses###########

from matplotlib import pyplot as plt

#fig,ax = plt.subplots(1,2,figsize=(8,3))

fig = plt.figure()
fig.patch.set_facecolor('white')
fig.patch.set_alpha(0.7)

#ax = fig.add_subplot(111)
#ax.patch.set_facecolor('white')
#ax.patch.set_alpha(1.0)
#ax.set_yscale('log')

plt.plot(training_loss_vs_epoch,label='training')
plt.plot(validation_loss_vs_epoch,label='validation')

plt.xlabel('Epochs')
plt.ylabel('Loss')

plt.legend()

plt.savefig('loss_vs_epoch.png')

