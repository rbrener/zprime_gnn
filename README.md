# Zprime_GNN

Standalone project to train and test a GNN on a Zprime->mumu+jb/jbjb signal against a ttbar background. These can be generalised.

Sequence of commands:


source setup

python DataLoaderImplementation.py ####Uses custom dataloader to convert .root files to dgl graphs which are saved into .bin files###

python PostSaveLoad.py ###Loads .bin files, processes data, saves .pt files###

python Collate_and_Train.py ###Loads .pt files, collates graphs, puts graphs on cuda machine, loads model, trains NN, saves trained model###

python Predict_Test_Sample.py ###Loads trained NN (model), loads .pt test file, tests NN's performance on test file, saves .jsons of targets, predictions, event indeces###
